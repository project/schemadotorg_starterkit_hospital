Table of contents
-----------------

* Introduction
* Features
* Notes
* Todo


Introduction
------------

The **Schema.org Blueprints Starter Kit: Hospital module** provides
Schema.org types required to build a hospital website.


Features
--------

- Provides Person (person), Physician (physician), Hospital (hospital) 
  and MedicalClinic (clinic) content types.
- Creates an /doctors and /locations view.
- Adds a default shortcut to view Doctors (/doctors) and Locations (/locations).


Notes
-----

### Concepts

- Businesses have employees
- Organizations have members
- Employees have job titles
- Members have roles

### Types

- **Person** (node:Person)  
  A person (alive, dead, undead, or fictional).  
  <https://schema.org/Person>

- **Medical Organization** (node:MedicalOrganization)  
  A medical organization (physical or not), such as hospital, institution or clinic.  
  <https://schema.org/MedicalOrganization>

- **Medical Business** (node:MedicalBusiness)  
  A particular physical or virtual business of an organization for medical purposes.  
  <https://schema.org/MedicalBusiness>

- **Hospital** (node:Hospital)  
  A hospital.  
  <https://schema.org/Hospital>

- **Clinic** (node:MedicalClinic)  
  A facility, often associated with a hospital or medical school, that is devoted to the specific diagnosis and/or healthcare.  
  <https://schema.org/MedicalClinic>

- **Physician** (node:Physician)  
  A doctor's office.  
  <https://schema.org/Physician>

- **Basic page** (node:WebPage)  
  A web page.  
  <https://schema.org/WebPage>

- **Special Announcement** (block_content:SpecialAnnouncement)  
  A SpecialAnnouncement combines a simple date-stamped textual information update with contextualized Web links and other structured data..  
  <https://schema.org/SpecialAnnouncement>


### Properties

- **contactPoint** - Used by Person, MedicalBusiness, Hospital, Physician, and MedicalClinic
- **employee** - Used by MedicalBusiness to organization People
- **member** - Used by MedicalOrganization to organization People
- **departments** - Not used and superseded by subOrganization / parentOrganization
- **subOrganization / parentOrganization** - Used by an Organization to organize Organizes.
